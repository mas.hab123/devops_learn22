#!/bin/bash
#webserver of linux - ubuntu 20.04
aws ec2 run-instances \
    --image-id ami-0851b76e8b1bce90b \
    --instance-type t2.micro \
    --count 1 \
    --subnet-id subnet-ap-east-1a \
    --key-name DEVOPS \
    --security-group-ids sg-0f061fa520a7a1e43 \
    --tag-specifications 'ResourceType=instance,Tags=[{Key=webserver,Value=linuxserver}]' 'ResourceType=volume,Tags=[{Key=cost-center,Value=cc123}]'
    --user-data file://install-utilitysoftwares.txt